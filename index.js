const inquirer = require('inquirer')
const { frameworks } = require('./questions/frameworks.json')
const { templates } = require('./questions/templates.json')
const { navigations } = require('./questions/navigations.json')
const { navArchitectures } = require('./questions/nav-architectures.json')
const { libraries } = require('./questions/libraries.json')

const { BuildProcess } = require('./builder')

inquirer
    .prompt([
        // Select framework
        {
            type: 'list',
            message: 'Select your framework',
            name: 'framework',
            choices: frameworks
        },
        // Select template for this framework
        {
            type: 'list',
            message: 'Select your template',
            name: 'template',
            choices: templates,
            when: (answers) => templates.find((t) => t.framework === answers.framework)
        },
        // Select navigation
        {
            type: 'list',
            message: 'Select your navigation library',
            name: 'navigation',
            choices: navigations,
            when: (answers) => navigations.find((t) => t.framework === answers.framework)
        },
        // Select navigation architecture
        {
            type: 'list',
            message: 'Select your navigation architecture',
            name: 'nav_architecture',
            choices: navArchitectures,
            when: (answers) => navArchitectures.find((t) => t.framework === answers.framework)
        },
        // Select external libraries
        {
            type: 'checkbox',
            message: 'Select your external libraries',
            name: 'libraries',
            choices: libraries,
            when: (answers) => libraries.find((t) => t.framework === answers.framework)
        }
    ])
    .then((answers) => {
        console.log('ANSWERS', answers)
        BuildProcess(answers)
        return answers
    })
